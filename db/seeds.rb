# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
c = Course.create name: 'Алгоритмы и анализ сложности'
3.times do |section_idx|
  s = c.sections.create name: "Section #{section_idx}"
  4.times do |theme_idx|
    t = s.themes.create name: "Theme #{section_idx}-#{theme_idx}"
    5.times do |question_idx|
      q = t.questions.create title: "Question #{section_idx}-#{theme_idx}-#{question_idx}"
      20.times do |answer_idx|
        correct = [true, false].sample
        q.answers.create text: correct, correct: correct
      end
    end
  end
end
Student.create email: 'test@example.comm'
