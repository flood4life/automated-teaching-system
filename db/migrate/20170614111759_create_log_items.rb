class CreateLogItems < ActiveRecord::Migration[5.1]
  def change
    create_table :log_items do |t|
      t.uuid :student_id, foreign_key: true
      t.uuid :section_id, foreign_key: true
      t.uuid :theme_id, foreign_key: true
      t.string :subtype
      t.jsonb :metadata

      t.timestamps
    end
  end
end
