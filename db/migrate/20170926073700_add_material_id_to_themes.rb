class AddMaterialIdToThemes < ActiveRecord::Migration[5.1]
  def change
    add_column :themes, :material_id, :uuid, foreign_key: true
  end
end
