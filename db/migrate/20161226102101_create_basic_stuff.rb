class CreateBasicStuff < ActiveRecord::Migration[5.1]
  def change
    enable_extension 'pgcrypto'

    create_table :courses, id: :uuid do |t|
      t.string :name
      t.timestamps
    end

    create_table :sections, id: :uuid do |t|
      t.string :name
      t.uuid :course_id, index: true
      t.timestamps
    end

    create_table :themes, id: :uuid do |t|
      t.string :name
      t.uuid :section_id, index: true
      t.timestamps
    end

    create_table :questions, id: :uuid do |t|
      t.string :title
      t.text :description
      t.uuid :theme_id, index: true
      t.timestamps
    end

    create_table :answers, id: :uuid do |t|
      t.string :text
      t.boolean :correct
      t.uuid :question_id, index: true
      t.timestamps
    end
  end
end
