class CreateMaterials < ActiveRecord::Migration[5.1]
  def change
    create_table :materials, id: :uuid do |t|
      t.string :title
      t.text :content
      t.uuid :section_id, foreign_key: true

      t.timestamps
    end
  end
end
