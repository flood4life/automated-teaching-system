class Material < ApplicationRecord
  belongs_to :section
  has_many :themes
end
