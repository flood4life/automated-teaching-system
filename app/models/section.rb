class Section < ApplicationRecord
  belongs_to :course
  has_many :themes
  has_many :materials

  has_many :log_items

  validates_presence_of :name
end
