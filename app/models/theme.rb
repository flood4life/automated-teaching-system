class Theme < ApplicationRecord
  belongs_to :section
  belongs_to :material
  has_many :questions

  def random_questions(n = 1)
    questions.sample(n)
  end
end
