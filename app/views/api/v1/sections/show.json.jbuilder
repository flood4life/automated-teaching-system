json.partial! 'api/v1/sections/section', section: @section
json.themes @section.themes, partial: 'api/v1/themes/theme', as: :theme
json.materials @section.materials, partial: 'api/v1/materials/material', as: :material