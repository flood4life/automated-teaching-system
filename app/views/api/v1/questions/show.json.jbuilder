json.partial! 'api/v1/questions/question', question: @question
json.answers @question.answers, partial: 'api/v1/answers/answer', as: :answer
