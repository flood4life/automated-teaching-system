json.partial! 'api/v1/themes/theme', theme: @theme
if true # if admin
  json.questions @theme.questions, partial: 'api/v1/questions/question', as: :question
else
  json.questions @theme.random_questions
end
