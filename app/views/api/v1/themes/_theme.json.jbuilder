json.name theme.name
json.id theme.id
json.question_count theme.questions.count
json.material theme.material, partial: 'api/v1/materials/material', as: :material