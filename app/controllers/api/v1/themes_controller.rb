class Api::V1::ThemesController < ApplicationController
  before_action :set_theme, only: [:show, :update, :destroy]

  def show
  end

  def create
    section = Section.find(params[:section_id])
    @theme = section.themes.new(theme_params)
    if @theme.save
      render 'show', status: :created
    else
      render json: @theme.errors.messages, status: :unprocessable_entity
    end
  rescue => e
    render json: e.message, status: :bad_request
  end

  def update
    @theme.update(theme_params)
    if @theme.save
      render 'update', status: :created
    else
      render json: @theme.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    if @theme.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def set_theme
    @theme = Theme.find(params[:id])
  end

  def theme_params
    params.tap{ |p| p[:theme][:material_id] = p[:materialId]}.require(:theme).permit(:name, :material_id)
  end
end
