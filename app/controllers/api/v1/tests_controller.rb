class Api::V1::TestsController < ApplicationController
  def question
    section = Section.find(params[:section_id])
    r = Services::QuestionForStudent.new(@student, section).call
    render json: r
  end

  def answer
    section = Section.find(params[:section_id])
    answer = params[:answer]
    r = Services::AnswerPostMachine.new(@student, section, answer).call
    render json: r
  end
end
