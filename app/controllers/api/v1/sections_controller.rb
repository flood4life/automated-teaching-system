class Api::V1::SectionsController < ApplicationController
  before_action :set_section, only: [:show, :update, :destroy]

  def index
    @sections = @course.sections
  end

  def show
  end

  def create
    @section = @course.sections.new(section_params)
    if @section.save
      render 'show', status: :created
    else
      render json: section.errors.messages, status: :unprocessable_entity
    end
  end

  def update
    @section.update(section_params)
    if @section.save
      render 'update', status: :created
    else
      render json: @section.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    if @section.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def set_section
    @section = Section.find(params[:id])
  end

  def section_params
    params.require(:section).permit(:name)
  end
end
