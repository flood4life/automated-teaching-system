class Api::V1::AnswersController < ApplicationController
  before_action :set_answer, only: [:update, :destroy]

  def create
    question = Question.find(params[:question_id])
    answer = question.answers.new(answer_params)
    if answer.save
      render json: answer, status: :created
    else
      render json: answer.errors.messages, status: :unprocessable_entity
    end
  rescue => e
    render json: e.message, status: :bad_request
  end

  def update
    @answer.update(answer_params)
    if @answer.save
      render json: @answer, status: :created
    else
      render json: @answer.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    if @answer.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def set_answer
    @answer = Answer.find(params[:id])
  end

  def answer_params
    params.require(:answer).permit(:text, :correct)
  end
end
