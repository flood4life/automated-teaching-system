class Api::V1::MaterialsController < ApplicationController
  before_action :set_material, only: [:show, :update, :destroy]

  def show
  end

  def create
    section = Section.find(params[:section_id])
    @material = section.materials.new(material_params)
    if @material.save
      render 'show', status: :created
    else
      render json: @material.errors.messages, status: :unprocessable_entity
    end
  rescue => e
    render json: e.message, status: :bad_request
  end

  def update
    @material.update(theme_params)
    if @material.save
      render 'update', status: :created
    else
      render json: @material.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    if @material.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def set_material
    @material = Material.find(params[:id])
  end

  def material_params
    params.require(:material).permit(:title, :content)
  end
end
