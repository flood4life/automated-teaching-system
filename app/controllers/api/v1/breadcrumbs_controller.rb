class Api::V1::BreadcrumbsController < ApplicationController
  def index
    if params[:klass].include?('student')
      render json: []
    else
      render json: Services::BreadcrumbsGenerator.new(params[:klass].constantize, params[:id]).call
    end
  end
end
