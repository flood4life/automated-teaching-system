class Api::V1::QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :update, :destroy]

  def show
  end

  def create
    theme = Theme.find(params[:theme_id])
    @question = theme.questions.new(question_params)
    if @question.save
      render 'show', status: :created
    else
      render json: @question.errors.messages, status: :unprocessable_entity
    end
  rescue => e
    render json: e.message, status: :bad_request
  end

  def update
    @question.update(question_params)
    if @question.save
      render 'update', status: :created
    else
      render json: @question.errors.messages, status: :unprocessable_entity
    end
  end

  def destroy
    if @question.destroy
      head :ok
    else
      head :unprocessable_entity
    end
  end

  private

  def set_question
    @question = Question.find(params[:id])
  end

  def question_params
    params.require(:question).permit(:title, :description)
  end
end
