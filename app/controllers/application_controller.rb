class ApplicationController < ActionController::API
  before_action :set_course, :set_student

  def set_course
    @course = Course.first
  end

  def set_student
    @student = Student.first
  end
end
