module Services
  class BreadcrumbsGenerator
    attr_reader :klass, :id
    
    def initialize(klass, id)
      @klass = klass
      @id = id
    end
    
    def call
      a = array.reverse
      a.map do |entity|
        res = {}
        res[:type] = entity.class.to_s
        res[:id] = entity.id
        res[:text] = entity[:name] || entity[:title]
        res
      end
    end

    def array
      parent_klass, symbol = parent_klass_and_symbol
      if klass == Course
        [Course.first]
      else
        instance = klass.find(id)
        [instance] + Services::BreadcrumbsGenerator.new(parent_klass, instance.send(symbol).id).array
      end
    end
    
    private
    
    def parent_klass_and_symbol
      if klass == Section
        [Course, :course]
      elsif klass == Theme
        [Section, :section]
      elsif klass == Question
        [Theme, :theme]
      end
    end
  end
end