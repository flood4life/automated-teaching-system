module Services
  class AnswerPostMachine
    attr_reader :student, :section, :answer

    def initialize(student, section, answer)
      @student = student
      @section = section
      @answer  = answer
    end

    def call
      r = Services::CheckSubmittedAnswer.new(student, section, answer).call
      malformed, has_incorrect, has_missed_correct = r
      all_fine = r.none?
      q_service = Services::QuestionForStudent.new(student, section)
      last_stats = q_service.last_stats
      stats = Services::ThemeProgressMachine.new(last_stats['remaining'], last_stats['streak'], all_fine).call
      remaining, streak, should_change = stats

      status, subtype   = if all_fine
                            ['closed', :question]
                          elsif should_change
                            ['failed', :question]
                          else
                            ['open', :answer]
                          end
      metadata          = {
        answer:    answer,
        status:    status,
        stats: {
          remaining: remaining,
          streak:    streak
        }
      }
      LogItem.create!(student_id: student.id, theme_id: q_service.current_theme.id, subtype: subtype, section_id: section.id, metadata: metadata)

      message = if has_incorrect && has_missed_correct
                  'Answer has incorrect and not all correct'
                elsif has_incorrect
                  'Answer has incorrect'
                elsif has_missed_correct
                  'Answer does not have all correct'
                else
                  'Correct!'
                end
      message = 'Malformed request' if malformed

      if remaining <= 0
        q_service.close_theme(metadata[:stats])
      end

      {malformed: malformed, message: message, correct: all_fine, should_change: should_change || all_fine,
       stats: {
         streak: streak,
         remaining: remaining
       }
      }
    end
  end
end