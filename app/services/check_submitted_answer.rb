module Services
  class CheckSubmittedAnswer
    attr_reader :student, :section, :answer

    def initialize(student, section, answer)
      @student = student
      @section = section
      @answer = answer
    end

    def call
      question = Services::QuestionForStudent.new(student, section).call
      correct_answers = {}
      Answer.find(question.metadata['answers'].map{|a| a['id']}).each do |a|
        correct_answers[a.id] = {correct: a.correct, visited: false}
      end
      has_incorrect_answers = false
      has_missing_correct_answers = false
      answer_malformed = false
      answer.each do |id, ans|
        unless correct_answers.key?(id)
          answer_malformed = true
          break
        end
        correct_answers[id][:visited] = true
        if correct_answers[id][:correct] && !ans
          has_missing_correct_answers = true
        elsif !correct_answers[id][:correct] && ans
          has_incorrect_answers = true
        end
      end
      answer_malformed ||= correct_answers.any? {|key, hash| !hash[:visited]}
      [answer_malformed, has_incorrect_answers, has_missing_correct_answers]
    end
  end
end