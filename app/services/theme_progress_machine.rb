module Services
  class ThemeProgressMachine
    attr_reader :remaining, :streak, :correct

    def initialize(remaining, streak, correct)
      @remaining = remaining
      @streak = streak
      @correct = correct
    end

    def call
      update_streak
      update_remaining
      [remaining, streak, should_switch_question]
    end

    private

    def update_streak
      if correct
        if streak >= 0
          @streak += 1
        else
          @streak = 1
        end
      else
        if streak <= 0
          @streak -= 1
        else
          @streak = -1
        end
      end
    end

    def update_remaining
      if correct
        @remaining -= 1
      else
        @remaining += 1
      end
      @remaining -= streak_modifier * sign(streak)
    end

    def sign(number)
      number <=> 0
    end

    def streak_modifier
      Math.log2(streak.abs).floor
    end

    def should_switch_question
      correct || streak.even?
    end
  end
end