module Services
  class QuestionForStudent
    attr_reader :student, :section

    def initialize(student, section)
      @student = student
      @section = section
    end

    def call
      if has_open_question?
        last_open_section_question
      else
        question = choose_next_question
        answers  = question.answers.sample(6)
        metadata = {
          status:  'open',
          id:      question.id,
          answers: answers.map do |a|
            { id: a.id, text: a.text }
          end,
          section: {
            name: section.name
          },
          theme:   {
            name: current_theme.name
          },
          question: {
            title: question.title,
            description: question.description
          },
          stats: last_stats || {
            'remaining' => 3,
            'streak' => 0
          }
        }
        LogItem.create!(student_id: student.id, subtype: :question, section_id: section.id, theme_id: current_theme.id, metadata: metadata)
      end
    end

    def last_stats
      student.log_items.where(section_id: section.id, theme_id: current_theme.id).last&.metadata&.dig('stats')
    end

    def close_theme(stats)
      LogItem.create!(
        student_id: student.id,
        subtype: :theme,
        section_id: section.id,
        theme_id: current_theme.id,
        metadata: {
          status: :closed,
          id: current_theme.id,
          stats: stats}
      )
    end

    def current_theme
      @theme ||= if has_open_theme?
                   Theme.find(last_open_section_theme.metadata['id'])
                 else
                   choose_next_theme
                 end
    end

    private

    def has_open_question?
      entity_status(last_open_section_question) == 'open'
    end

    def last_open_section_question
      student.log_items.where(section_id: section.id, subtype: :question).last
    end

    def choose_next_question
      current_theme.questions.sample
    end

    def choose_next_theme
      section.themes.where.not(id: closed_themes).first
    end

    def closed_themes
      student.log_items.where(section_id: section.id, subtype: :theme).where("metadata->>'status' = 'closed'").pluck("metadata->>'id'")
    end

    def has_open_theme?
      entity_status(last_open_section_theme) == 'open'
    end

    def last_open_section_theme
      student.log_items.where(section_id: section.id, subtype: :theme).last
    end

    def entity_status(entity)
      entity&.metadata&.dig('status')
    end

  end
end
