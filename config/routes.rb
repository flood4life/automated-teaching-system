Rails.application.routes.draw do
  namespace :api, path: '/api', defaults: { format: :json } do
    scope module: :v1 do
      resources :sections do
        resources :themes, shallow: true do
          resources :questions, shallow: true do
            resources :answers, shallow: true
          end
        end
        resources :materials, shallow: true
      end

      get '/breadcrumbs' => 'breadcrumbs#index'
      get '/tests/question'
      post '/tests/answer'
    end
  end
end
